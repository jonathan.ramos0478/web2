<?php
 /**
  *
  */
 class Seminarios extends CI_Controller
 {

   function __construct()
   {
     parent::__construct();
     //Cargar el modelo
     $this->load->model('Seminario');
     if(!$this->session->userdata("conectado")){
       	redirect("welcome/login");
     	}
   }

   public function index()
   {
     $data['seminarios']=$this->Seminario->obtenerTodos();

     $this->load->view('header');
     $this->load->view('seminarios/index',$data);
     $this->load->view('footer');
   }

   public function nuevo()
   {
     $this->load->view('header');
     $this->load->view('seminarios/nuevo');
     $this->load->view('footer');
   }

   public function guardar(){
     $datosNuevoSeminario= array(
       "nombre_jbrp"=>$this->input->post('nombre_jbrp'),
       "duracion_jbrp"=>$this->input->post('duracion_jbrp'),
       "costo_jbrp"=>$this->input->post('costo_jbrp'),
      );

//para subir fotos

      $this->load->library("upload");
              $new_name = "contenido_jbrp_" . time() . "_" . rand(1, 5000);
              $config['file_name'] = $new_name . '_1';
              $config['upload_path']          = FCPATH . 'uploads/';
              $config['allowed_types']        = 'pdf';
              $config['max_size']             = 1024*5;
              $this->upload->initialize($config);

              if ($this->upload->do_upload("contenido_jbrp")) {
                $dataSubida = $this->upload->data();
                $datosNuevoSeminario["contenido_jbrp"] = $dataSubida['file_name'];
              }


      if ($this->Seminario->insertar($datosNuevoSeminario)){

        $this->session->set_flashdata("confirmacion","Seminario guardado exitosamente");
      }else {
      $this->session->set_flashdata("error","Error al guardar intente de nuevo");
      }
        redirect ('seminarios/index');

   }


         public function eliminar($id_jbrp){
            if ($this->session->userdata("conectado")->perfil_usu!="ADMINISTRADOR") {
              $this->session->set_flashdata("error", "no tiene permisos para eliminar");
              redirect("seminarios/index");
            }

           if ($this->Seminario->borrar($id_jbrp)) {
             $this->session->set_flashdata("confirmacion","Seminario eliminado exitosamente");
           }else {
             $this->session->set_flashdata("error","Seminario guardado exitosamente");
           }
           redirect ('seminarios/index');
         }

//funcion renderizar vista editar con el instructor
        public function editar ($id_jbrp){
          $data["seminarioEditar"]=
          $this->Seminario->obtenerPorId($id_jbrp);
          $this->load->view('header');
          $this->load->view('seminarios/editar',$data);
          $this->load->view('footer');


        }
// proceso de actualizacion

        public function procesarActualizacion(){
            $datosEditados= array(
              "nombre_jbrp"=>$this->input->post('nombre_jbrp'),
              "duracion_jbrp"=>$this->input->post('duracion_jbrp'),
              "costo_jbrp"=>$this->input->post('costo_jbrp'),

             );
          $id_jbrp=$this->input->post("id_jbrp");
          if ($this->Seminario->actualizar($id_jbrp,$datosEditados)) {
            $this->session->set_flashdata("confirmacion","Seminario editado exitosamente");
          }else {
            $this->session->set_flashdata("error","Seminario error");
          }
          redirect ('seminarios/index');
        }



}




 ?>
