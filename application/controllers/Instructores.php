<?php
 /**
  *
  */
 class Instructores extends CI_Controller
 {

   function __construct()
   {
     parent::__construct();
     //Cargar el modelo
     $this->load->model('Instructor');
     if(!$this->session->userdata("conectado")){
       	redirect("welcome/login");
     	}
   }

   public function index()
   {
     $data['instructores']=$this->Instructor->obtenerTodos();

     $this->load->view('header');
     $this->load->view('instructores/index',$data);
     $this->load->view('footer');
   }

   public function nuevo()
   {
     $this->load->view('header');
     $this->load->view('instructores/nuevo');
     $this->load->view('footer');
   }

   public function guardar(){
     $datosNuevoInstructor= array(
       "cedula_ins"=>$this->input->post('cedula_ins'),
       "primer_apellido_ins"=>$this->input->post('primer_apellido_ins'),
       "segundo_apellido_ins"=>$this->input->post('segundo_apellido_ins'),
       "nombres_ins"=>$this->input->post('nombres_ins'),
       "titulo_ins"=>$this->input->post('titulo_ins'),
       "telefono_ins"=>$this->input->post('telefono_ins'),
       "direccion_ins"=>$this->input->post('direccion_ins')

      );

//para subir fotos

      $this->load->library("upload");
              $new_name = "foto_instructor_" . time() . "_" . rand(1, 5000);
              $config['file_name'] = $new_name . '_1';
              $config['upload_path']          = FCPATH . 'uploads/';
              $config['allowed_types']        = 'jpeg|jpg|png';
              $config['max_size']             = 1024*5;
              $this->upload->initialize($config);

              if ($this->upload->do_upload("foto_ins")) {
                $dataSubida = $this->upload->data();
                $datosNuevoInstructor["foto_ins"] = $dataSubida['file_name'];
              }


      if ($this->Instructor->insertar($datosNuevoInstructor)){

        $this->session->set_flashdata("confirmacion","Instructor guardado exitosamente");
      }else {
      $this->session->set_flashdata("error","Error al guardar intente de nuevo");
      }
        redirect ('instructores/index');

   }


         public function eliminar($id_ins){
            if ($this->session->userdata("conectado")->perfil_usu!="ADMINISTRADOR") {
              $this->session->set_flashdata("error", "no tiene permisos para eliminar");
              redirect("instructores/index");
            }

           if ($this->Instructor->borrar($id_ins)) {
             $this->session->set_flashdata("confirmacion","Instructor eliminado exitosamente");
           }else {
             $this->session->set_flashdata("error","Instructor guardado exitosamente");
           }
           redirect ('instructores/index');
         }

//funcion renderizar vista editar con el instructor
        public function editar ($id_ins){
          $data["instructorEditar"]=
          $this->Instructor->obtenerPorId($id_ins);
          $this->load->view('header');
          $this->load->view('instructores/editar',$data);
          $this->load->view('footer');


        }
// proceso de actualizacion

        public function procesarActualizacion(){
            $datosEditados= array(
              "cedula_ins"=>$this->input->post('cedula_ins'),
              "primer_apellido_ins"=>$this->input->post('primer_apellido_ins'),
              "segundo_apellido_ins"=>$this->input->post('segundo_apellido_ins'),
              "nombres_ins"=>$this->input->post('nombres_ins'),
              "titulo_ins"=>$this->input->post('titulo_ins'),
              "telefono_ins"=>$this->input->post('telefono_ins'),
              "direccion_ins"=>$this->input->post('direccion_ins')

             );
          $id_ins=$this->input->post("id_ins");
          if ($this->Instructor->actualizar($id_ins,$datosEditados)) {
            $this->session->set_flashdata("confirmacion","Instructor editado exitosamente");
          }else {
            $this->session->set_flashdata("error","Instructor error");
          }
          redirect ('instructores/index');
        }



}




 ?>
