<?php
 /**
  *
  */
 class Docentes extends CI_Controller
 {

   function __construct()
   {
     parent::__construct();
     //Cargar el modelo
     $this->load->model('Docente');
   }

   public function index()
   {
     $data['docentes']=$this->Docente->obtenerTodos();
     $this->load->view('header');
     $this->load->view('docentes/index',$data);
     $this->load->view('footer');
   }

   public function nuevo()
   {
     $this->load->view('header');
     $this->load->view('docentes/nuevo');
     $this->load->view('footer');
   }

   public function guardar(){
     $datosNuevoDocente= array(
       "nombre_docente"=>$this->input->post('nombre_docente'),
       "apellido_docente"=>$this->input->post('apellido_docente'),
       "asignatura_docente"=>$this->input->post('asignatura_docente')
     );

      //$this->Instructor->insertar($datosNuevoInstructor);
      if ($this->Docente->insertar($datosNuevoDocente))
      {
        redirect ('docentes/index');
      }else {
        echo "Error al insertar";
      }


   }


         public function eliminar($id_docente){
           if ($this->Docente->borrar($id_docente)) {
             redirect('docentes/index');
           }else {
             echo "error al borrar";
           }
         }






}





 ?>
