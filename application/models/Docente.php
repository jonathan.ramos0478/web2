<?php

  class Docente extends CI_Model
  {

    function __construct()
    {
      parent::__construct();
    }


    function insertar($data){
        //Active record en CodeIgniter
        return $this->db->insert('docente',$data);
    }

    function obtenerTodos(){
        $listadoInstructores=$this->db->get("docente");
        if ($listadoInstructores->num_rows()>0) {

          return $listadoInstructores->result();
        } else {
          return false;
        }


    }

     public function borrar($id_docente)
    {
      $this->db->where("id_docente",$id_docente);
      return $this->db->delete("docente");
    }
  }




 ?>
