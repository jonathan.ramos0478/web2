<?php

  class Seminario extends CI_Model
  {

    function __construct()
    {
      parent::__construct();
    }


    function insertar($datos){
        //Active record en CodeIgniter
        return $this->db->insert('seminario',$datos);
    }

    function obtenerTodos(){
        $listadoSeminarios=$this->db->get("seminario");
        if ($listadoSeminarios->num_rows()>0) {

          return $listadoSeminarios->result();
        } else {
          return false;
        }


    }

     public function borrar($id_jbrp)
    {
      $this->db->where("id_jbrp",$id_jbrp);
      return $this->db->delete("seminario");
    }

//funcion para consultar un isntructor
    function obtenerPorId($id_jbrp){
      $this->db->where("id_jbrp",$id_jbrp);
      $seminario=$this->db->get("seminario");
      if ($seminario->num_rows()>0){
        return $seminario->row();
      }
      return false;
}
//funcion para actualizar instructor

    function actualizar($id_jbrp,$datos){
      $this->db->where("id_jbrp",$id_jbrp);
      return $this->db->update('seminario',$datos);
}

  }
 ?>
