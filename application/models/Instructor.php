<?php

  class Instructor extends CI_Model
  {

    function __construct()
    {
      parent::__construct();
    }


    function insertar($datos){
        //Active record en CodeIgniter
        return $this->db->insert('instructor',$datos);
    }

    function obtenerTodos(){
        $listadoInstructores=$this->db->get("instructor");
        if ($listadoInstructores->num_rows()>0) {

          return $listadoInstructores->result();
        } else {
          return false;
        }


    }

     public function borrar($id_ins)
    {
      $this->db->where("id_ins",$id_ins);
      return $this->db->delete("instructor");
    }

//funcion para consultar un isntructor
    function obtenerPorId($id_ins){
      $this->db->where("id_ins",$id_ins);
      $instructor=$this->db->get("instructor");
      if ($instructor->num_rows()>0){
        return $instructor->row();
      }
      return false;
}
//funcion para actualizar instructor

    function actualizar($id_ins,$datos){
      $this->db->where("id_ins",$id_ins);
      return $this->db->update('instructor',$datos);
}

  }
 ?>
