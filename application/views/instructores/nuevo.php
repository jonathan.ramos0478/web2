<h1>Nuevo instructores</h1>

<form class="" id="frm_nuevo_instructor" action="<?php echo site_url(); ?>/instructores/guardar" method="post" enctype="multipart/form-data">

  <div class="row">
    <div class="col-md-4">

       <label for="">Cedula:
       <span class="obligatorio">(Obligatorio)</span>
       </label>
       <br>
       <input type="number" class="form-control" required min="99999999" name="cedula_ins" value="" id="cedula_ins" placeholder="Ingrese su cedula">

    </div>
    <div class="col-md-5">

      <label for="">Primer Apellido:
      <span class="obligatorio">(Obligatorio)</span>
    </label>
      <br>
      <input type="text" class="form-control"required name="primer_apellido_ins" value="" id="primer_apellido_ins " placeholder="Ingrese su primer Apellido">

    </div>
    <div class="col-md-3">
      <label for="">Segundo Apellido:
    </label>
      <br>
      <input type="text" class="form-control"name="segundo_apellido_ins" id="segundo_apellido_ins" value=""  placeholder="Ingrese su segundo Apellido">

    </div>

  </div>

  <div class="row">

    <div class="col-md-4">
      <label for="">Nombre
      <span class="obligatorio">(Obligatorio)</span>
    </label>
      <br>
      <input type="text" class="form-control" required name="nombres_ins" value="" id="nombres_ins" placeholder="Ingrese su snombre">


    </div>
    <div class="col-md-5">
      <label for="">Titulo:
      <span class="obligatorio">(Obligatorio)</span>
    </label>
      <br>
      <input type="text" class="form-control" required name="titulo_ins" value=""  id="titulo_ins" placeholder="Ingrese su titulo">
    </div>

    <div class="col-md-3">
      <label for="">Telefono:
      <span class="obligatorio">(Obligatorio)</span>
    </label>
      <br>
      <input type="number" class="form-control" required name="telefono_ins" value=""  id="telefono_ins" placeholder="Ingrese su telefono">
    </div>

  </div>

  <div class="row">
    <div class="col-md-12">
        <label for="">Direccion:
        <span class="obligatorio">(Obligatorio)</span>
      </label>
        <br>
        <input type="text" class="form-control" required name="direccion_ins" id="direccion_ins" value=""  placeholder="Ingrese la Direccion">
    </div>
  </div>

  <br>
  <div class="row">
    <div class="col-md-4">
      <label for="">Foto</label>
      <input type="file" name="foto_ins" id="foto_ins">
    </div>
  </div>

  <div class="row">
    <div class="col-md-12 text-center">

      <button type="submit" name="button" class="btn btn-primary">Guardar</button>&nbsp;
      <a href="<?php echo site_url(); ?>/instructores/index" class="btn btn-danger">Cancelar </a>
    </div>

  </div>

</form>


<script type="text/javascript">

  $("#frm_nuevo_instructor").validate({
    rules:{
        cedula_ins:{
          required: true,
          minlength:10,
          maxlength:10,
          digist: true,
        },
      primer_apellido_ins:{
        required: true,
        minlength:3,
        maxlength:250,
        letras: true,

      },
      nombres_ins:{
        required: true,
        minlength:3,
        maxlength:15,
        letras: true,
      },
      titulo_ins:{
        required: true,
        minlength:3,
        maxlength:250,
      },
      telefono_ins:{
        required: true,
        minlength:10,
        maxlength:10,
        digist: true,
      },
      direccion_ins:{
        required: true,
        minlength:2,
        maxlength:250,
      },
    },

    messages:{
      cedula_ins:{
        required: "porfavor ingrese el numero de cedula",
        minlength:"cedula incorrecta, ingrese 10 digitos",
        maxlength:"cedula incorrecta, ingrese 10 digitos",
        digits: "solo acepta numeros",
        number: "Este campo solo acepta números",
      },
    primer_apellido_ins:{
      required: "ingrese el primer apellido",
      minlength:"El apellido debe tener 3 caracteres",
      maxlength:"Apellido incorrecto",

    },
    nombres_ins:{
      required: "ingrese su nombre",
      minlength:"El nombre debe tener 3 caracteres",
      maxlength:"Nombre incorrecto",

    },
    titulo_ins:{
      required: "ingrese su titulo",
      minlength:"El titulo debe tener 3 caracteres",
      maxlength:"Titulo incorrecto",
    },
    telefono_ins:{
      required: "ingrese su telefono",
      minlength:"Telefono incorrecto ingrese 9 digitos",
      maxlength:"Telefono incorrecto ingrese 9 digitos",
      digist: "solo acepta numeros",
      number: "Este campo solo acepta numeros",
    },
    direccion_ins:{
      required: "ingrese su Direccion",
      minlength:"La direccion debe tener 3 caracteres",
      maxlength:"Direccion incorrecto",
    },

  },

});


</script>

<script type="text/javascript">
  $("#foto_ins").fileinput();

</script>
