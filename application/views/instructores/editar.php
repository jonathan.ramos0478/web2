<h1>Editar instructores</h1>

<form class="" id="frm_editar_instructor" action="<?php echo site_url('instructores/procesarActualizacion'); ?>" method="post">

  <div class="row">
    <input type="hidden" name="id_ins" value="<?php echo $instructorEditar->id_ins; ?>">

    <div class="col-md-4">

       <label for="">Cedula:
       <span class="obligatorio">(Obligatorio)</span>
       </label>
       <br>
       <input type="number" class="form-control" name="cedula_ins" value="<?php echo $instructorEditar->cedula_ins; ?>" id="cedula_ins" placeholder="Ingrese su cedula">

    </div>
    <div class="col-md-5">

      <label for="">Primer Apellido:
      <span class="obligatorio">(Obligatorio)</span>
    </label>
      <br>
      <input type="text" class="form-control"required name="primer_apellido_ins" value="<?php echo $instructorEditar->primer_apellido_ins; ?>" id="primer_apellido_ins " placeholder="Ingrese su primer Apellido">

    </div>
    <div class="col-md-3">
      <label for="">Segundo Apellido:
    </label>
      <br>
      <input type="text" class="form-control"name="segundo_apellido_ins" id="segundo_apellido_ins" value="<?php echo $instructorEditar->segundo_apellido_ins; ?>"  placeholder="Ingrese su segundo Apellido">

    </div>

  </div>

  <div class="row">

    <div class="col-md-4">
      <label for="">Nombre
      <span class="obligatorio">(Obligatorio)</span>
    </label>
      <br>
      <input type="text" class="form-control" required name="nombres_ins" value="<?php echo $instructorEditar->nombres_ins; ?>" id="nombres_ins" placeholder="Ingrese su snombre">


    </div>
    <div class="col-md-5">
      <label for="">Titulo:
      <span class="obligatorio">(Obligatorio)</span>
    </label>
      <br>
      <input type="text" class="form-control" required name="titulo_ins" value="<?php echo $instructorEditar->titulo_ins; ?>"  id="titulo_ins" placeholder="Ingrese su titulo">
    </div>

    <div class="col-md-3">
      <label for="">Telefono:
      <span class="obligatorio">(Obligatorio)</span>
    </label>
      <br>
      <input type="number" class="form-control" required name="telefono_ins" value="<?php echo $instructorEditar->telefono_ins; ?>"  id="telefono_ins" placeholder="Ingrese su telefono">
    </div>

  </div>

  <div class="row">
    <div class="col-md-12">
        <label for="">Direccion:
        <span class="obligatorio">(Obligatorio)</span>
      </label>
        <br>
        <input type="text" class="form-control" required name="direccion_ins" id="direccion_ins" value="<?php echo $instructorEditar->direccion_ins; ?>"  placeholder="Ingrese la Direccion">
    </div>
  </div>
  <br>
  <div class="row">
    <div class="col-md-12 text-center">

      <button type="submit" name="button" class="btn btn-primary">Editar</button>&nbsp;
      <a href="<?php echo site_url(); ?>/instructores/index" class="btn btn-danger">Cancelar </a>
    </div>

  </div>

</form>


<script type="text/javascript">

  $("#frm_editar_instructor").validate({
    rules:{
        cedula_ins:{
          required: true,
          minlength:10,
          maxlength:10,
          digist: true,
        },
      primer_apellido_ins:{
        required: true,
        minlength:3,
        maxlength:250,
        letras: true,

      },
      nombres_ins:{
        required: true,
        minlength:3,
        maxlength:15,
        letras: true,
      },
      titulo_ins:{
        required: true,
        minlength:3,
        maxlength:250,
      },
      telefono_ins:{
        required: true,
        minlength:10,
        maxlength:10,
        digist: true,
      },
      direccion_ins:{
        required: true,
        minlength:2,
        maxlength:250,
      },
    },

    messages:{
      cedula_ins:{
        required: "porfavor ingrese el numero de cedula",
        minlength:"cedula incorrecta, ingrese 10 digitos",
        maxlength:"cedula incorrecta, ingrese 10 digitos",
        digits: "solo acepta numeros",
        number: "Este campo solo acepta números",
      },
    primer_apellido_ins:{
      required: "ingrese el primer apellido",
      minlength:"El apellido debe tener 3 caracteres",
      maxlength:"Apellido incorrecto",

    },
    nombres_ins:{
      required: "ingrese su nombre",
      minlength:"El nombre debe tener 3 caracteres",
      maxlength:"Nombre incorrecto",

    },
    titulo_ins:{
      required: "ingrese su titulo",
      minlength:"El titulo debe tener 3 caracteres",
      maxlength:"Titulo incorrecto",
    },
    telefono_ins:{
      required: "ingrese su telefono",
      minlength:"Telefono incorrecto ingrese 9 digitos",
      maxlength:"Telefono incorrecto ingrese 9 digitos",
      digist: "solo acepta numeros",
      number: "Este campo solo acepta numeros",
    },
    direccion_ins:{
      required: "ingrese su Direccion",
      minlength:"La direccion debe tener 3 caracteres",
      maxlength:"Direccion incorrecto",
    },

  },

});


</script>
