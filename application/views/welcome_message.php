<!DOCTYPE html>
<html lang="es" dir="ltr">
	<head>
		<meta charset="utf-8">
		<title></title>
	</head>
	<body>
		  <h1>Bienvenidos</h1>
			<div id="myCarousel" class="carousel slide" data-ride="carousel">
			  <!-- Indicators -->
			  <ol class="carousel-indicators">
			    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
			    <li data-target="#myCarousel" data-slide-to="1"></li>
			    <li data-target="#myCarousel" data-slide-to="2"></li>
			  </ol>

			  <!-- Wrapper for slides -->
			  <div class="carousel-inner">
			    <div class="item active">
			      <img src="<?php  echo base_url(); ?>/assets/images/fond1png.jpeg" alt="Baner 1" height="500px" width="500px" >
			    </div>

			    <div class="item">
			      <img src="<?php echo base_url(); ?>/assets/images/fond2.png" alt="Baner 2" height="500px" width="500px">
			    </div>

			    <div class="item">
			      <img src="<?php echo base_url(); ?>/assets/images/fond3.png" alt="Baner 3" height="500px" width="500px">
			    </div>
			  </div>

			  <!-- Left and right controls -->
			  <a class="left carousel-control" href="#myCarousel" data-slide="prev">
			    <span class="glyphicon glyphicon-chevron-left"></span>
			    <span class="sr-only">Previous</span>
			  </a>
			  <a class="right carousel-control" href="#myCarousel" data-slide="next">
			    <span class="glyphicon glyphicon-chevron-right"></span>
			    <span class="sr-only">Next</span>
			  </a>
			</div>
	</body>
</html>
