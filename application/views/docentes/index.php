<h1>LISTADO DE docente</h1>
<br>
<?php if ($docentes): ?>
    <table class="table table-striped
    table-bordered table-hover">
        <thead>
           <tr>
             <th>ID</th>
             <th>NOMBRE</th>
             <th>APELLIDO</th>
             <th>ASIGNATURA</th>
           </tr>
        </thead>
        <tbody>
            <?php foreach ($docentes
            as $filaTemporal): ?>
              <tr>
                  <td>
                      <?php echo $filaTemporal->id_docente; ?>
                  </td>
                  <td>  <?php echo $filaTemporal->nombre_docente?></td>
                  <td>  <?php echo $filaTemporal->apellido_docente ?></td>
                  <td><?php echo $filaTemporal->asignatura_docente?></td>
                  <td class="text-center">
                    <a href="#" title="Editar Instructor" style="color:black;">
                      <i class="glyphicon glyphicon-pencil"></i>
                    </a>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                    <a href="<?php echo site_url();?>/docentes/eliminar/<?php echo $filaTemporal->id_docente; ?>"title="Eliminar Instructor" style="color:red;">
                      <i class="glyphicon glyphicon-trash"></i>
                    </a>
                  </td>


              </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
<?php else: ?>
  <h1>No hay instructores</h1>
<?php endif; ?>
