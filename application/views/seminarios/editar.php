<h1>Editar instructores</h1>

<form class="" id="frm_editar_seminario" action="<?php echo site_url('seminarios/procesarActualizacion'); ?>" method="post">

  <div class="row">
    <input type="hidden" name="id_jbrp" value="<?php echo $seminarioEditar->id_jbrp; ?>">

    <div class="col-md-4">

       <label for="">Nombre:
       <span class="obligatorio">(Obligatorio)</span>
       </label>
       <br>
       <input type="text" class="form-control" name="nombre_jbrp" value="<?php echo $seminarioEditar->nombre_jbrp; ?>" id="nombre_jbrp" placeholder="Ingrese su nombre">

    </div>
    <div class="col-md-5">

      <label for="">Duración:
      <span class="obligatorio">(Obligatorio)</span>
    </label>
      <br>
      <input type="text" class="form-control"required name="duracion_jbrp" value="<?php echo $seminarioEditar->duracion_jbrp; ?>" id="duracion_jbrp " placeholder="Ingrese la duración">

    </div>
    <div class="col-md-3">
      <label for="">Costo:
    </label>
      <br>
      <input type="text" class="form-control"name="costo_jbrp" id="segundo_apellido_ins" value="<?php echo $seminarioEditar->costo_jbrp; ?>"  placeholder="Ingrese el costo">

    </div>

  </div>

  <br>
  <div class="row">
    <div class="col-md-12 text-center">

      <button type="submit" name="button" class="btn btn-primary">Editar</button>&nbsp;
      <a href="<?php echo site_url(); ?>/seminarios/index" class="btn btn-danger">Cancelar </a>
    </div>

  </div>

</form>


<script type="text/javascript">

  $("#frm_editar_seminario").validate({
    rules:{

      nombres_jbrp:{
        required: true,
        minlength:3,
        maxlength:15,
        letras: true,
      },
      duracion_jbrp:{
        required: true,
        minlength:3,
        maxlength:4,
      },
      costo_jbrp:{
        required: true,
        minlength:1,
        maxlength:10,
        digist: true,
      },
    },

    messages:{
    nombres_jbrp:{
      required: "ingrese su nombre",
      minlength:"El nombre debe tener 3 caracteres",
      maxlength:"Nombre incorrecto",

    },
    duracion_jbrp:{
      required: "ingrese la duración",
      minlength:"La duración debe tener 3 caracteres",
      maxlength:"Duración incorrecto",
    },
    costo_jbrp:{
      required: "ingrese el costo",
      minlength:"costo incorrecto ingrese 1 dígito",
      maxlength:"Telefono incorrecto ingrese 10 dígitos",
      digist: "solo acepta numeros",
      number: "Este campo solo acepta numeros",
    },

  },

});


</script>
