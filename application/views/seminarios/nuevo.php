<h1>Nuevo seminario</h1>

<form class="" id="frm_nuevo_seminario" action="<?php echo site_url(); ?>/seminarios/guardar" method="post" enctype="multipart/form-data">

  <div class="row">
    <div class="col-md-4">

       <label for="">Nombre:
       <span class="obligatorio">(Obligatorio)</span>
       </label>
       <br>
       <input type="text" class="form-control" required name="nombre_jbrp" value="" id="nombre_jbrp" placeholder="Ingrese su Nombre">

    </div>
    <div class="col-md-5">

      <label for="">Duración:
      <span class="obligatorio">(Obligatorio)</span>
    </label>
      <br>
      <input type="text" class="form-control"required name="duracion_jbrp" value="" id="duracion_jbrp " placeholder="Ingrese la duracion">

    </div>
    <div class="col-md-3">
      <label for="">Costo:
    </label>
      <br>
      <input type="text" class="form-control"name="costo_jbrp" id="costo_jbrp" value=""  placeholder="Ingrese su costo">

    </div>

  </div>

  <br>
  <div class="row">
    <div class="col-md-6">
      <label for="">Contenido</label>
      <input type="file" name="contenido_jbrp" id="contenido_jbrp">
    </div>
  </div>

  <div class="row">
    <div class="col-md-12 text-center">

      <button type="submit" name="button" class="btn btn-primary">Guardar</button>&nbsp;
      <a href="<?php echo site_url(); ?>/seminarios/index" class="btn btn-danger">Cancelar </a>
    </div>

  </div>

</form>


<script type="text/javascript">

  $("#frm_nuevo_seminario").validate({
    rules:{

      nombres_jbrp:{
        required: true,
        minlength:3,
        maxlength:15,
        letras: true,
      },
      duracion_jbrp:{
        required: true,
        minlength:3,
        maxlength:4,
      },
      costo_jbrp:{
        required: true,
        minlength:1,
        maxlength:10,
        digist: true,
      },
    },

    messages:{

    nombres_jbrp:{
      required: "ingrese su nombre",
      minlength:"El nombre debe tener 3 caracteres",
      maxlength:"Nombre incorrecto",

    },
    duracion_jbrp:{
      required: "ingrese la duración",
      minlength:"La duración debe tener 3 caracteres",
      maxlength:"Duración incorrecto",
    },
    costo_jbrp:{
      required: "ingrese el costo",
      minlength:"costo incorrecto ingrese 1 dígito",
      maxlength:"Telefono incorrecto ingrese 10 dígitos",
      digist: "solo acepta numeros",
      number: "Este campo solo acepta numeros",
    },
  },

});


</script>

<script type="text/javascript">
  $("#contenido_jbrp").fileinput();

</script>
