<div class="row">
  <div class="col-md-8">
    <h1>LISTADO DE SEMINARIOS</h1>
  </div>
  <div class="col-md-4">
    <a href="<?php echo site_url('seminarios/nuevo'); ?>" class="btn btn-primary"><i class="glyphicon glyphicon-plus"></i>agregar instructor</a>

  </div>
</div>



<br>
<?php if ($seminarios): ?>
    <table class="table table-striped
    table-bordered table-hover" id="tbl_seminarios">
        <thead>
           <tr>
             <th>ID</th>
             <th>CONTENIDO</th>
             <th>NOMBRE</th>
             <th>DURACION</th>
             <th>COSTO</th>
             <th>ACCIONES</th>
           </tr>
        </thead>
        <tbody>
            <?php foreach ($seminarios
            as $filaTemporal): ?>
              <tr>
                  <td><?php echo $filaTemporal->id_jbrp; ?></td>
                  <td>
                    <?php if ($filaTemporal->contenido_jbrp!=""): ?>
                      <img src="<?php echo base_url('uploads/').$filaTemporal->contenido_jbrp ?>" alt="">
                    <?php else: ?>
                      N/A
                    <?php endif; ?>
                  <td><?php echo $filaTemporal->nombre_jbrp?></td>
                  <td><?php echo $filaTemporal->duracion_jbrp ?></td>
                  <td><?php echo $filaTemporal->costo_jbrp ?></td>
                  <td class="text-center">
                    <a href="<?php echo site_url();?>/seminarios/editar/<?php echo $filaTemporal->id_jbrp; ?>" title="Editar Instructor" style="color:green;">
                      <i class="mdi mdi-pencil"></i>
                      Editar
                    </a>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                    <?php if ($this->session->userdata("conectado")->perfil_usu== "ADMINISTRADOR"): ?>
                      <a href="<?php echo site_url();?>/seminarios/eliminar/<?php echo $filaTemporal->id_jbrp; ?>"title="Eliminar Seminario" onclick="return confirm('deseas eliminar');" style="color:red;">
                        <i class="mdi mdi-close"></i>
                        eliminar
                      </a>
                    <?php endif; ?>
                  </td>
              </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
<?php else: ?>
  <h1>No hay instructores</h1>
<?php endif; ?>

<script type="text/javascript">
  $("#tbl_seminarios").DataTable();
</script>
